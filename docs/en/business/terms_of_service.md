# Wayne OS Terms of Service

## Note
Original document: [이용약관.md](https://gitlab.com/wayne-inc/wayneos/-/blob/master/docs/ko/%EB%B9%84%EC%A6%88%EB%8B%88%EC%8A%A4/%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80.md)

## Article 1 (Definition, purpose, and deemed agreement of the terms of service)
① “TOS” refers to Wayne OS Terms of service..
<br>② “Company” refers to Wayne Inc.
<br>③ “Wayne OS” refers to all kinds of OS related source code, binary, document, etc. that are made or managed by Wayne Inc.
<br>④ This TOS is intended to stipulate the necessary terms, procedures, etc., and rights and obligations between Company and you for your use of Wayne OS.
<br>⑤ If you conduct one or more than the following sub-clauses by Wayne OS, you are bound to agree to this TOS.
```
1. Acquire, Reacquire 
2. Download, Upload
3. Installation, Reinstallation
4. Use
5. Modification
6. Expansion
7. Copy
8. Transfer, Retransfer
9. Distribution, Redistribution
10. Purchase, Repurchase
11. Sale, Resale
12. Lease, Rent, Lend, Borrow
13. Translation, Interpretation
14. Publication, Announcement
15. Etc.
```

## Article 2 (Intellectual property)
① Company has all the rights including patent rights, copyrights, trademarks, etc. intellectual properties related to Wayne OS, and are protected by relevant laws (Korean laws take precedence) and international agreements.
<br>② Conduct any actions of article 1 clause 5, does not transfer all rights, including intellectual property rights of Wayne OS to you, and you do not acquire any rights from Company except the limited use rights of Wayne OS granted under this TOS.
<br>③ Anything in this TOS should not be interpreted as granting you the rights beyond the limited use rights granted under this TOS.

## Article 3 (Open source)
① Wayne OS may contain specific softwares or libraries using open source software. The license for this open source software is granted to you under its own applicable license terms.
<br>② Information and details of each license can be found on the Wayne OS Open Source site (wayne-os.com > Open source) or Chromium OS Open Source site (chromium.org/chromium-os), which can be changed by the licensors without any notice.
<br>③ If the open source license of each package or file conflicts with this TOS, the open source software license takes precedence to the extent to which it is in conflict.
<br>④ Under the open source license terms, you may receive a copy of the open source files through the Wayne OS Open Source site (if a copy of the open source files have not been posted on the site due to circumstances, it may be received through the contacts in this TOS).
<br>⑤ Open source software and Wayne OS are distributed for useful use, but do not provide any form of warranties including the implied warranties of merchantability or fitness for a particular use.
<br>⑥ Company, contributors, open source licensors, suppliers, etc. do not bear any liability for any direct/indirect loss/damage caused by your use of open source software or Wayne OS to the maximum extent permitted by law, regardless of whether or not they were aware of the possibility of loss/damage. 

## Article 4 (Your rights and responsibilities)
① Only upon your agreement to this TOS during your use of Wayne OS, you are granted a non-exclusive license about Wayne OS by the company according to this TOS, and may conduct any actions of article 1 clause 5 under this TOS.
<br>② Company is not responsible for the consequences of your Wayne OS activities, and if Company suffers loss/damage from your Wayne OS activities, you must indemnify Company for the loss/damage.
<br>③ You shall not transfer the right to use Wayne OS, assign the right to use Wayne OS and any other contractual status related to this TOS, without the written consent of Company.
<br>④ You shall not use Wayne OS to defame Company and third parties, produce antisocial products, or engage in any illegal activities that may violate related laws or social trade rules.
<br>⑤ You are responsible for all your actions, and you shall not engage in any activities that make third parties misunderstand you represent Company.

## Article 5 (Other restrictions)
① In principle, you shall not infringe intellectual property rights of Wayne OS and Company. However, creating secondary works using Wayne OS is allowed to the extent that it doesn't infringe the intellectual property rights of Wayne OS and Company.
<br>② You shall not remove or modify rights information related to this TOS, indicators for intellectual rights, mark, label, etc. in Wayne OS products.
<br>③ If you modify and disclose Wayne OS, you must disclose that Wayne OS is modified by you so that the modified Wayne OS is not misunderstood as the original Wayne OS, and you must change at least one of mentioned conditions including the splash-screen or brand identity (logo, name). If you don’t agree with this, you cannot disclose the modified Wayne OS. 
<br>④ Except as permitted in this TOS, you may not do anything by all or part of Wayne OS, without the written consent of Company.

## Article 6 (Company’s warranty and limitation of responsibility)
① Company and Wayne OS contributors are not responsible for any loss/damages caused by Wayne OS to the maximum extent permitted by relevant laws.
<br>② Company and suppliers (including agencies, agents, executives, employees, etc.) do not provide any guarantees, including marketability, suitability for a specific purpose, intellectual property rights or implied guarantees for non-infringement of intellectual property, etc. to the maximum extent permitted by relevant laws.
<br>③ Company does not warrant that Wayne OS satisfies your requirements and does not warrant not to cause temporary interruption or error for computer use, and Company disclaims all warranties, which may be disclaimed to the maximum extent permitted by applicable laws.
<br>④ Company may continue to fix bugs, update to fix errors and improve performance of Wayne OS, but this is not the Company's obligation and may be stopped at the discretion of the Company
<br>⑤ Company shall not be responsible for the problems or your loss/damages caused by computer hardware, application, virus, worm, harmful program, malicious code, etc.
<br>⑥ Company has the judgment right to determine what features may adversely affect the computer system, which program files or etc. components may adversely affect the user computer system, and Company shall not be responsible for loss/damages caused by error in judgment.
<br>⑦ When you conduct any actions of article 1 clause 5 of Wayne OS, Company may directly provide you advertisements, contents, and other services about Company or third party’s services, and the clause 1 through 6 will be applied equally to the advertisements, contents, and other services.

## Article 7 (Collection of information and privacy policy)
① Company complies with related laws such as the "Act on Promotion of Information and Communication Network Utilization and Information Protection" and the "Personal Information Protection Act" in the Republic of Korea.
<br>② For Wayne OS download statistics and usage statistics, Company may collect the following information found in the course of your use of Wayne OS and Wayne OS website, and the information does not correspond to the ‘personal information’ pursuant to article 2 sub-clause 1 under the Personal Information Protection Act.
```
1. External and internal internet protocol addresses of the device that is used to download Wayne OS (region verification)
2. Download time (automatically and immediately saved to the server)
```
<br>③ Except each sub-clause of the clause 2, Company does not collect any of your ‘personal information’ pursuant to article 2 sub-clause 1 under the Personal Information Protection Act.
<br>④ The legal review has been completed that the information stipulated in each sub-clause of clause 2 does not correspond to the ‘personal information’ under article 2 sub-clause 1 under the Personal Information Protection Act, and if your personal information is collected unintentionally, Company will immediately destroy it and will not disclose it to the outside.

## Article 8 (Other contents)
① You should be aware that contents provided to you, such as advertisements posted on Wayne OS website or Wayne OS and sponsor contents included in the service, may be protected by intellectual property rights owned by sponsors or advertisers that provide it to Google (or any other person or company on behalf of Google). You may not modify, rent, sell, distribute, create secondary works based on all or some contents unless specifically permitted by Google or its content owner in a separate contract.
<br>② You understand and agree that Company does not guarantee any advertisements or other contents posted on Wayne OS website or Wayne OS, and that Company shall not be responsible for any loss/damages that may occur to you as a result of your trust in the completeness, accuracy or existence of advertisements and other contents posted on Wayne OS website or Wayne OS.

## Article 9 (Termination of contract and restriction of use)
① You may terminate this TOS at any time by permanently deleting or discarding Wayne OS relevant components that include the originals and copies.
<br>② In any of the following sub-clauses, this TOS about you will be terminated automatically, and you must delete or discard permanently Wayne OS relevant components that include the originals and copies.
```
1. In the case you do not comply with this TOS or relevant laws and regulations
2. In the case you commit an offense or lawsuit against Wayne OS, Company, or Wayne OS Contributors
3. In the case Company decides to suspend services due to business suspension, etc.
```
<br>③ In the case you caused any loss/damages to Company, Company may sue compensation for loss/damages to you beside the termination of this TOS.
 
## Article 10 (Governing law and jurisdiction)
① This TOS governing law is the law of the Republic of Korea.
<br>② If any legal dispute or lawsuit is instituted, the court having jurisdiction over the location of Company headquarters at the time of filing shall have exclusive jurisdiction.

## Article 11 (Entire contract, etc.)
① We confirm that anything that is not described in this TOS is not agreed between Company and you.
<br>② If the jurisdiction court that can judge this case determines that the specific provision of this TOS is invalid, only that provision will be deleted from this TOS, and the rest of the provisions will continue to be valid and enforceable.
<br>③ Every agreement about Wayne OS (including understanding, statement, advertisement, and notice) including written or oral contract between you and Company will be replaced by this TOS, the contents of this TOS will take precedence if previous agreement and the contents of this TOS are different.
<br>④ The original TOS is written in Korean. Even if Korean TOS is provided to you as translations in other languages, it is only for your convenience, and the original TOS in Korean takes precedence in the relationship between you and Company.
<br>⑤ If you have any question about this TOS, please inquire at  Wayne OS forum site (wayne-os.com > Forum), or evelyn@wayne-inc.com (English, Indonesian inquiries), or seongbin@wayne-inc.com (English, Korean inquiries), or Insta1-510, 204, Convensia-daero, Yeonsu-gu, Incheon, 22004, Republic of Korea (mail).

## Addenda.
1. This TOS will take effect since 2022-01-01 (UTC+00:00)
2. The previous Wayne Software License Agreement will be replaced by this TOS.
