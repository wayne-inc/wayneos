## Note
This document is looking foward to your contribution (documentation, translation, reporting, suggestion, coding).

## Joining Google Groups
In order to login to Wayne OS by Google account, you have to add the account to Google's whitelist.
1. Login to your Google account.
2. Visit https://groups.google.com/u/0/a/chromium.org/g/google-browser-signin-testaccounts
3. Press _Join group_ button
4. Login with your Google account in Wayne OS.

## Note
Above method is for test purpose and managed by Google officially. 
<br>
However, It doesn't mean that you can use all of Google services since Google restricts/controls third-parties of open source.
<br>

## Reference 
https://wayne-os.com/googles-restriction-for-chromium-chromium-os
<br>
https://www.chromium.org/developers/how-tos/api-keys
